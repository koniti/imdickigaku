# 九星気学風水の辞書
  
mozc_kigaku.txt        Goole日本語入力。UTF-8  
make_ios_plist.sh	macosの辞書にインポートするファイルを作成。引数はmozc_kigaku.txtフォーマットのファイル名。  
  
Apache v2 License  

changelog:
2023-12-15
- msime_kigaku_utf8.txt、msime_kigaku_sjis.txt はもう作成しません。
- mozc_kigaku.txtファイルがtab separateファイルです。表計算ソフトなどに読み込んで、MS-IME用に加工してください。  

2015年時点のMS-IMEのフォーマット(2023年時点で同じか不明):
```
!Microsoft IME Dictionary Tool
!Version:
!Format:WORDLIST
!User Dictionary Name:
!Output File Name:
!DateTime:

よみ	単語	品詞
```