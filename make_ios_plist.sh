#!env bash

cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
EOF

while read yomi tango remain
do
  if [ "x$yomi" = "x" ]; then continue; fi
  if [ "x$tango" = "x" ]; then continue; fi
  j=${yomi/</&lt;}
  k=${j/>/&gt;}
  yomi="${k}"
  j=${tango/</&lt;}
  k=${j/>/&gt;}
  tango="${k}"
  echo "	<dict>"
  echo "		<key>phrase</key>"
  echo "		<string>${tango}</string>"
  echo "		<key>shortcut</key>"
  echo "		<string>${yomi}</string>"
  echo "	</dict>"
done < "${1:-/dev/stdin}"

cat <<EOF
</array>
</plist>
EOF

exit

: <<EOF
$0 mozc_kigaku.txt > /tmp/dict.plist
cat mozc_kigaku.txt | $0 > /tmp/dict.plist
EOF
